class Article < ApplicationRecord
  validates_date :published_on, on_or_after: lambda { Date.current }
end
